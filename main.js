#!/usr/bin/env node

const cacache = require('cacache');
const fs = require('fs');
const cp = require('child_process');

const args = process.argv.slice(2); // skip node and main.js
const action = args[0];
const outDir = args[1];

const cachePath = `${outDir}/_cacache/`;

(async () => {
    let paths;
    switch (action) {
        case "build":
            const metadata = require(args[2]);
            paths = args.slice(3);

            for (let i in metadata) {
                const entry = metadata[i]; // TODO: this is now an array
                const data = await fs.promises.readFile(paths[i]);
                for (const variant of entry) {
                    let [algorithm] = variant.integrity.split('-', 1);
                    let actual_hash = await cacache.put(cachePath, variant.key, data, {
                        metadata: variant.metadata,
                        size: variant.size,
                        time: variant.time,
                        algorithms: [algorithm],
                    });
                    if (actual_hash != variant.integrity) {
                        console.log(`hash mismatch: expected ${variant.integrity} found ${actual_hash}`);
                        process.exit(1);
                    }
                }
            }

            break;

        case "merge":
            paths = args.slice(2);

            for (let path of paths) {
                await fs.promises.cp(`${path}/_cacache/`, "cache", { recursive: true });
                cp.execSync("chmod -R 700 cache");

                const keys = Object.keys(await cacache.ls("cache"));

                for (let key of keys) {
                    const data = (await cacache.get("cache", key)).data;
                    const entry = await cacache.index.compact("cache", key, () => false, { validateEntry: () => true });

                    for (const variant of entry) {
                        let [algorithm] = variant.integrity.split('-', 1);
                        let actual_hash = await cacache.put(cachePath, key, data, {
                            metadata: variant.metadata,
                            size: variant.size,
                            time: variant.time,
                            algorithms: [algorithm],
                        });
                        if (actual_hash != variant.integrity) {
                            console.log(`hash mismatch: expected ${variant.integrity} found ${actual_hash}`);
                            process.exit(1);
                        }
                    }
                }

                await fs.promises.rm("cache", { recursive: true });
            }

            break;

        default:
            console.error("unknown action " + action);
            process.exit(1);
            break;
    }

    process.exit(0);
})();