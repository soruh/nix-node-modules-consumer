# This file has been generated by node2nix 1.11.1. Do not edit!

{nodeEnv, fetchurl, fetchgit, nix-gitignore, stdenv, lib, globalBuildInputs ? []}:

let
  sources = {
    "@npmcli/fs-3.1.0" = {
      name = "_at_npmcli_slash_fs";
      packageName = "@npmcli/fs";
      version = "3.1.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/@npmcli/fs/-/fs-3.1.0.tgz";
        sha512 = "7kZUAaLscfgbwBQRbvdMYaZOWyMEcPTH/tJjnyAWJ/dvvs9Ef+CERx/qJb9GExJpl1qipaDGn7KqHnFGGixd0w==";
      };
    };
    "aggregate-error-3.1.0" = {
      name = "aggregate-error";
      packageName = "aggregate-error";
      version = "3.1.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/aggregate-error/-/aggregate-error-3.1.0.tgz";
        sha512 = "4I7Td01quW/RpocfNayFdFVk1qSuoh0E7JrbRJ16nH01HhKFQ88INq9Sd+nd72zqRySlr9BmDA8xlEJ6vJMrYA==";
      };
    };
    "balanced-match-1.0.2" = {
      name = "balanced-match";
      packageName = "balanced-match";
      version = "1.0.2";
      src = fetchurl {
        url = "https://registry.npmjs.org/balanced-match/-/balanced-match-1.0.2.tgz";
        sha512 = "3oSeUO0TMV67hN1AmbXsK4yaqU7tjiHlbxRDZOpH0KW9+CeX4bRAaX0Anxt0tx2MrpRpWwQaPwIlISEJhYU5Pw==";
      };
    };
    "brace-expansion-2.0.1" = {
      name = "brace-expansion";
      packageName = "brace-expansion";
      version = "2.0.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/brace-expansion/-/brace-expansion-2.0.1.tgz";
        sha512 = "XnAIvQ8eM+kC6aULx6wuQiwVsnzsi9d3WxzV3FpWTGA19F621kwdbsAcFKXgKUHZWsy+mY6iL1sHTxWEFCytDA==";
      };
    };
    "cacache-17.0.4" = {
      name = "cacache";
      packageName = "cacache";
      version = "17.0.4";
      src = fetchurl {
        url = "https://registry.npmjs.org/cacache/-/cacache-17.0.4.tgz";
        sha512 = "Z/nL3gU+zTUjz5pCA5vVjYM8pmaw2kxM7JEiE0fv3w77Wj+sFbi70CrBruUWH0uNcEdvLDixFpgA2JM4F4DBjA==";
      };
    };
    "chownr-2.0.0" = {
      name = "chownr";
      packageName = "chownr";
      version = "2.0.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/chownr/-/chownr-2.0.0.tgz";
        sha512 = "bIomtDF5KGpdogkLd9VspvFzk9KfpyyGlS8YFVZl7TGPBHL5snIOnxeshwVgPteQ9b4Eydl+pVbIyE1DcvCWgQ==";
      };
    };
    "clean-stack-2.2.0" = {
      name = "clean-stack";
      packageName = "clean-stack";
      version = "2.2.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/clean-stack/-/clean-stack-2.2.0.tgz";
        sha512 = "4diC9HaTE+KRAMWhDhrGOECgWZxoevMc5TlkObMqNSsVU62PYzXZ/SMTjzyGAFF1YusgxGcSWTEXBhp0CPwQ1A==";
      };
    };
    "fs-minipass-2.1.0" = {
      name = "fs-minipass";
      packageName = "fs-minipass";
      version = "2.1.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/fs-minipass/-/fs-minipass-2.1.0.tgz";
        sha512 = "V/JgOLFCS+R6Vcq0slCuaeWEdNC3ouDlJMNIsacH2VtALiu9mV4LPrHc5cDl8k5aw6J8jwgWWpiTo5RYhmIzvg==";
      };
    };
    "fs-minipass-3.0.1" = {
      name = "fs-minipass";
      packageName = "fs-minipass";
      version = "3.0.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/fs-minipass/-/fs-minipass-3.0.1.tgz";
        sha512 = "MhaJDcFRTuLidHrIttu0RDGyyXs/IYHVmlcxfLAEFIWjc1vdLAkdwT7Ace2u7DbitWC0toKMl5eJZRYNVreIMw==";
      };
    };
    "fs.realpath-1.0.0" = {
      name = "fs.realpath";
      packageName = "fs.realpath";
      version = "1.0.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/fs.realpath/-/fs.realpath-1.0.0.tgz";
        sha512 = "OO0pH2lK6a0hZnAdau5ItzHPI6pUlvI7jMVnxUQRtw4owF2wk8lOSabtGDCTP4Ggrg2MbGnWO9X8K1t4+fGMDw==";
      };
    };
    "glob-8.1.0" = {
      name = "glob";
      packageName = "glob";
      version = "8.1.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/glob/-/glob-8.1.0.tgz";
        sha512 = "r8hpEjiQEYlF2QU0df3dS+nxxSIreXQS1qRhMJM0Q5NDdR386C7jb7Hwwod8Fgiuex+k0GFjgft18yvxm5XoCQ==";
      };
    };
    "imurmurhash-0.1.4" = {
      name = "imurmurhash";
      packageName = "imurmurhash";
      version = "0.1.4";
      src = fetchurl {
        url = "https://registry.npmjs.org/imurmurhash/-/imurmurhash-0.1.4.tgz";
        sha512 = "JmXMZ6wuvDmLiHEml9ykzqO6lwFbof0GG4IkcGaENdCRDDmMVnny7s5HsIgHCbaq0w2MyPhDqkhTUgS2LU2PHA==";
      };
    };
    "indent-string-4.0.0" = {
      name = "indent-string";
      packageName = "indent-string";
      version = "4.0.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/indent-string/-/indent-string-4.0.0.tgz";
        sha512 = "EdDDZu4A2OyIK7Lr/2zG+w5jmbuk1DVBnEwREQvBzspBJkCEbRa8GxU1lghYcaGJCnRWibjDXlq779X1/y5xwg==";
      };
    };
    "inflight-1.0.6" = {
      name = "inflight";
      packageName = "inflight";
      version = "1.0.6";
      src = fetchurl {
        url = "https://registry.npmjs.org/inflight/-/inflight-1.0.6.tgz";
        sha512 = "k92I/b08q4wvFscXCLvqfsHCrjrF7yiXsQuIVvVE7N82W3+aqpzuUdBbfhWcy/FZR3/4IgflMgKLOsvPDrGCJA==";
      };
    };
    "inherits-2.0.4" = {
      name = "inherits";
      packageName = "inherits";
      version = "2.0.4";
      src = fetchurl {
        url = "https://registry.npmjs.org/inherits/-/inherits-2.0.4.tgz";
        sha512 = "k/vGaX4/Yla3WzyMCvTQOXYeIHvqOKtnqBduzTHpzpQZzAskKMhZ2K+EnBiSM9zGSoIFeMpXKxa4dYeZIQqewQ==";
      };
    };
    "lru-cache-6.0.0" = {
      name = "lru-cache";
      packageName = "lru-cache";
      version = "6.0.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/lru-cache/-/lru-cache-6.0.0.tgz";
        sha512 = "Jo6dJ04CmSjuznwJSS3pUeWmd/H0ffTlkXXgwZi+eq1UCmqQwCh+eLsYOYCwY991i2Fah4h1BEMCx4qThGbsiA==";
      };
    };
    "lru-cache-7.14.1" = {
      name = "lru-cache";
      packageName = "lru-cache";
      version = "7.14.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/lru-cache/-/lru-cache-7.14.1.tgz";
        sha512 = "ysxwsnTKdAx96aTRdhDOCQfDgbHnt8SK0KY8SEjO0wHinhWOFTESbjVCMPbU1uGXg/ch4lifqx0wfjOawU2+WA==";
      };
    };
    "minimatch-5.1.6" = {
      name = "minimatch";
      packageName = "minimatch";
      version = "5.1.6";
      src = fetchurl {
        url = "https://registry.npmjs.org/minimatch/-/minimatch-5.1.6.tgz";
        sha512 = "lKwV/1brpG6mBUFHtb7NUmtABCb2WZZmm2wNiOA5hAb8VdCS4B3dtMWyvcoViccwAW/COERjXLt0zP1zXUN26g==";
      };
    };
    "minipass-3.3.6" = {
      name = "minipass";
      packageName = "minipass";
      version = "3.3.6";
      src = fetchurl {
        url = "https://registry.npmjs.org/minipass/-/minipass-3.3.6.tgz";
        sha512 = "DxiNidxSEK+tHG6zOIklvNOwm3hvCrbUrdtzY74U6HKTJxvIDfOUL5W5P2Ghd3DTkhhKPYGqeNUIh5qcM4YBfw==";
      };
    };
    "minipass-4.0.1" = {
      name = "minipass";
      packageName = "minipass";
      version = "4.0.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/minipass/-/minipass-4.0.1.tgz";
        sha512 = "V9esFpNbK0arbN3fm2sxDKqMYgIp7XtVdE4Esj+PE4Qaaxdg1wIw48ITQIOn1sc8xXSmUviVL3cyjMqPlrVkiA==";
      };
    };
    "minipass-collect-1.0.2" = {
      name = "minipass-collect";
      packageName = "minipass-collect";
      version = "1.0.2";
      src = fetchurl {
        url = "https://registry.npmjs.org/minipass-collect/-/minipass-collect-1.0.2.tgz";
        sha512 = "6T6lH0H8OG9kITm/Jm6tdooIbogG9e0tLgpY6mphXSm/A9u8Nq1ryBG+Qspiub9LjWlBPsPS3tWQ/Botq4FdxA==";
      };
    };
    "minipass-flush-1.0.5" = {
      name = "minipass-flush";
      packageName = "minipass-flush";
      version = "1.0.5";
      src = fetchurl {
        url = "https://registry.npmjs.org/minipass-flush/-/minipass-flush-1.0.5.tgz";
        sha512 = "JmQSYYpPUqX5Jyn1mXaRwOda1uQ8HP5KAT/oDSLCzt1BYRhQU0/hDtsB1ufZfEEzMZ9aAVmsBw8+FWsIXlClWw==";
      };
    };
    "minipass-pipeline-1.2.4" = {
      name = "minipass-pipeline";
      packageName = "minipass-pipeline";
      version = "1.2.4";
      src = fetchurl {
        url = "https://registry.npmjs.org/minipass-pipeline/-/minipass-pipeline-1.2.4.tgz";
        sha512 = "xuIq7cIOt09RPRJ19gdi4b+RiNvDFYe5JH+ggNvBqGqpQXcru3PcRmOZuHBKWK1Txf9+cQ+HMVN4d6z46LZP7A==";
      };
    };
    "minizlib-2.1.2" = {
      name = "minizlib";
      packageName = "minizlib";
      version = "2.1.2";
      src = fetchurl {
        url = "https://registry.npmjs.org/minizlib/-/minizlib-2.1.2.tgz";
        sha512 = "bAxsR8BVfj60DWXHE3u30oHzfl4G7khkSuPW+qvpd7jFRHm7dLxOjUk1EHACJ/hxLY8phGJ0YhYHZo7jil7Qdg==";
      };
    };
    "mkdirp-1.0.4" = {
      name = "mkdirp";
      packageName = "mkdirp";
      version = "1.0.4";
      src = fetchurl {
        url = "https://registry.npmjs.org/mkdirp/-/mkdirp-1.0.4.tgz";
        sha512 = "vVqVZQyf3WLx2Shd0qJ9xuvqgAyKPLAiqITEtqW0oIUjzo3PePDd6fW9iFz30ef7Ysp/oiWqbhszeGWW2T6Gzw==";
      };
    };
    "once-1.4.0" = {
      name = "once";
      packageName = "once";
      version = "1.4.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/once/-/once-1.4.0.tgz";
        sha512 = "lNaJgI+2Q5URQBkccEKHTQOPaXdUxnZZElQTZY0MFUAuaEqe1E+Nyvgdz/aIyNi6Z9MzO5dv1H8n58/GELp3+w==";
      };
    };
    "p-map-4.0.0" = {
      name = "p-map";
      packageName = "p-map";
      version = "4.0.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/p-map/-/p-map-4.0.0.tgz";
        sha512 = "/bjOqmgETBYB5BoEeGVea8dmvHb2m9GLy1E9W43yeyfP6QQCZGFNa+XRceJEuDB6zqr+gKpIAmlLebMpykw/MQ==";
      };
    };
    "promise-inflight-1.0.1" = {
      name = "promise-inflight";
      packageName = "promise-inflight";
      version = "1.0.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/promise-inflight/-/promise-inflight-1.0.1.tgz";
        sha512 = "6zWPyEOFaQBJYcGMHBKTKJ3u6TBsnMFOIZSa6ce1e/ZrrsOlnHRHbabMjLiBYKp+n44X9eUI6VUPaukCXHuG4g==";
      };
    };
    "semver-7.3.8" = {
      name = "semver";
      packageName = "semver";
      version = "7.3.8";
      src = fetchurl {
        url = "https://registry.npmjs.org/semver/-/semver-7.3.8.tgz";
        sha512 = "NB1ctGL5rlHrPJtFDVIVzTyQylMLu9N9VICA6HSFJo8MCGVTMW6gfpicwKmmK/dAjTOrqu5l63JJOpDSrAis3A==";
      };
    };
    "ssri-10.0.1" = {
      name = "ssri";
      packageName = "ssri";
      version = "10.0.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/ssri/-/ssri-10.0.1.tgz";
        sha512 = "WVy6di9DlPOeBWEjMScpNipeSX2jIZBGEn5Uuo8Q7aIuFEuDX0pw8RxcOjlD1TWP4obi24ki7m/13+nFpcbXrw==";
      };
    };
    "tar-6.1.13" = {
      name = "tar";
      packageName = "tar";
      version = "6.1.13";
      src = fetchurl {
        url = "https://registry.npmjs.org/tar/-/tar-6.1.13.tgz";
        sha512 = "jdIBIN6LTIe2jqzay/2vtYLlBHa3JF42ot3h1dW8Q0PaAG4v8rm0cvpVePtau5C6OKXGGcgO9q2AMNSWxiLqKw==";
      };
    };
    "unique-filename-3.0.0" = {
      name = "unique-filename";
      packageName = "unique-filename";
      version = "3.0.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/unique-filename/-/unique-filename-3.0.0.tgz";
        sha512 = "afXhuC55wkAmZ0P18QsVE6kp8JaxrEokN2HGIoIVv2ijHQd419H0+6EigAFcIzXeMIkcIkNBpB3L/DXB3cTS/g==";
      };
    };
    "unique-slug-4.0.0" = {
      name = "unique-slug";
      packageName = "unique-slug";
      version = "4.0.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/unique-slug/-/unique-slug-4.0.0.tgz";
        sha512 = "WrcA6AyEfqDX5bWige/4NQfPZMtASNVxdmWR76WESYQVAACSgWcR6e9i0mofqqBxYFtL4oAxPIptY73/0YE1DQ==";
      };
    };
    "wrappy-1.0.2" = {
      name = "wrappy";
      packageName = "wrappy";
      version = "1.0.2";
      src = fetchurl {
        url = "https://registry.npmjs.org/wrappy/-/wrappy-1.0.2.tgz";
        sha512 = "l4Sp/DRseor9wL6EvV2+TuQn63dMkPjZ/sp9XkghTEbV9KlPS1xUsZ3u7/IQO4wxtcFB4bgpQPRcR3QCvezPcQ==";
      };
    };
    "yallist-4.0.0" = {
      name = "yallist";
      packageName = "yallist";
      version = "4.0.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/yallist/-/yallist-4.0.0.tgz";
        sha512 = "3wdGidZyq5PB084XLES5TpOSRA3wjXAlIWMhum2kRcv/41Sn2emQ0dycQW4uZXLejwKvg6EsvbdlVL+FYEct7A==";
      };
    };
  };
  args = {
    name = "nix-node-modules-consumer";
    packageName = "nix-node-modules-consumer";
    version = "0.0.0";
    src = ./.;
    dependencies = [
      sources."@npmcli/fs-3.1.0"
      sources."aggregate-error-3.1.0"
      sources."balanced-match-1.0.2"
      sources."brace-expansion-2.0.1"
      sources."cacache-17.0.4"
      sources."chownr-2.0.0"
      sources."clean-stack-2.2.0"
      sources."fs-minipass-3.0.1"
      sources."fs.realpath-1.0.0"
      sources."glob-8.1.0"
      sources."imurmurhash-0.1.4"
      sources."indent-string-4.0.0"
      sources."inflight-1.0.6"
      sources."inherits-2.0.4"
      sources."lru-cache-7.14.1"
      sources."minimatch-5.1.6"
      sources."minipass-4.0.1"
      (sources."minipass-collect-1.0.2" // {
        dependencies = [
          sources."minipass-3.3.6"
        ];
      })
      (sources."minipass-flush-1.0.5" // {
        dependencies = [
          sources."minipass-3.3.6"
        ];
      })
      (sources."minipass-pipeline-1.2.4" // {
        dependencies = [
          sources."minipass-3.3.6"
        ];
      })
      (sources."minizlib-2.1.2" // {
        dependencies = [
          sources."minipass-3.3.6"
        ];
      })
      sources."mkdirp-1.0.4"
      sources."once-1.4.0"
      sources."p-map-4.0.0"
      sources."promise-inflight-1.0.1"
      (sources."semver-7.3.8" // {
        dependencies = [
          sources."lru-cache-6.0.0"
        ];
      })
      sources."ssri-10.0.1"
      (sources."tar-6.1.13" // {
        dependencies = [
          (sources."fs-minipass-2.1.0" // {
            dependencies = [
              sources."minipass-3.3.6"
            ];
          })
        ];
      })
      sources."unique-filename-3.0.0"
      sources."unique-slug-4.0.0"
      sources."wrappy-1.0.2"
      sources."yallist-4.0.0"
    ];
    buildInputs = globalBuildInputs;
    meta = {
    };
    production = true;
    bypassCache = true;
    reconstructLock = false;
  };
in
{
  args = args;
  sources = sources;
  tarball = nodeEnv.buildNodeSourceDist args;
  package = nodeEnv.buildNodePackage args;
  shell = nodeEnv.buildNodeShell args;
  nodeDependencies = nodeEnv.buildNodeDependencies (lib.overrideExisting args {
    src = stdenv.mkDerivation {
      name = args.name + "-package-json";
      src = nix-gitignore.gitignoreSourcePure [
        "*"
        "!package.json"
        "!package-lock.json"
      ] args.src;
      dontBuild = true;
      installPhase = "mkdir -p $out; cp -r ./* $out;";
    };
  });
}
